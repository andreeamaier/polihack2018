$(document).ready(function e(){
    $("#search_but").click(function f(){
        switch($("#search_el").val()) {
			case "for":
			{
				$("#for_res").show();
				$("#video-1").show();
				$("#video-2").hide();
				$("#video-3").hide();
				$("#switch_res").hide();
				$("#while_res").hide();
				$("#nothing_res").hide();
				$("#try-now").show();
				break;
			}
			case "switch":
			{
				$("#video-1").hide();
				$("#video-2").show();
				$("#video-3").hide();
				$("#switch_res").show();
				$("#while_res").hide();
				$("#for_res").hide();
				$("#nothing_res").hide();
				$("#try-now").show();
				break;
			}
			case "while":
			{
				$("#while_res").show();
				$("#video-1").hide();
				$("#video-2").hide();
				$("#video-3").show();
				$("#for_res").hide();
				$("#switch_res").hide();
				$("#nothing_res").hide();
				$("#try-now").show();
				break;
			}
			default:
			{
				$("#for_res").hide();
				$("#nothing_res").show();
				$("#switch_res").hide();
				$("#while_res").hide();
				$("#video-1").hide();
				$("#video-2").hide();
				$("#video-3").hide();
			}
}
    });
});